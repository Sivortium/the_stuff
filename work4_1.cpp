// work3_1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main()
{
    //length of a line
    const int LENGTH = 100;
    const int MAX_LINES = 50;

    //God, save the memory
    char Arr[MAX_LINES][LENGTH];
    char *pointers[MAX_LINES];

    int lines = 0;

    while (lines < MAX_LINES){
        printf("Type in your string: ");
        if (fgets(Arr[lines], LENGTH, stdin)[0] == '\n')
        {
            if (lines)
                break;
            else{
                printf("At least one is needed, do it again\n");
                fflush(stdin);
                continue;
            }
        }
        fflush(stdin);
        pointers[lines] = Arr[lines];
        lines++;
    }

    printf("\nHere you go:\n\n");

    for (int i = 0; i < lines; i++){
        int min = i;
        for (int i2 = i + 1; i2 < lines; i2++){
            if (strlen(pointers[min]) > strlen(pointers[i2])){
                min = i2;
            }
        }
        char *temp = pointers[i];
        pointers[i] = pointers[min];
        pointers[min] = temp;
        int outer = 0;
        while (*(pointers[i] + outer)){
            putchar(*(pointers[i] + outer++));
        }
    }

    return 0;
}