// work4_3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <string.h>
int main()
{
    char line[200];
    printf("Input a word to find out if it is a palindrome:\n");

    while (!scanf("%s", line)){
        fflush(stdin);
        printf("Something wrong, try again\n");
    }
    int ind = 0;
    const int LEN = strlen(line);
    for (; ind < LEN / 2; ind++){
        if (line[ind] != line[LEN - ind-1]){
            break;
        }
    }
    if (ind == LEN / 2)
        printf("yeap\n");
    else
        printf("nope\n");
    return 0;
}