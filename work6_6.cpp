// work6_6.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdlib.h>
int getFib(int n, int *arr)
{
    if (n > 1){
        //if it wasn't already proccessed, put the result in the array
        if (!arr[n - 1])
            arr[n - 1] = getFib(n - 1, arr) + getFib(n - 2, arr);
        return arr[n - 1];
    }
    //returns 0 or 1
    return n;
}
int getNthFib(int N)
{
    //an array for results
    int *arr = (int *)malloc(sizeof(int)* N);
    for (int t = 0; t < N; t++)
        arr[t] = 0;
    N = getFib(N, arr);
    free(arr);
    return N;
}
int main()
{
    int N = 0;
    while (!scanf("%d", &N))
    {
        fflush(stdin);
        printf("something is wrong, try again");
    }
    printf("F(%d)=%d\n", N, getNthFib(N));
    return 0;
}