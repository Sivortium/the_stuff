// work1_4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>

int main()
{
    int foot;
    int inch;
    const int inchesInFoot = 12;
    const float smInInch = 2.54;
    const int smInMeter = 100;
    float res;
    printf("Here you can transfer inches and foots into meters\nEnter feet\n");

    while (!scanf("%d", &foot)){
        printf("Something went wrong, try again\n");
        fflush(stdin);
    }
    printf("Enter inches\n");

    while (!scanf("%d", &inch)){
        printf("Something went wrong, try again\n");
        fflush(stdin);
    }
    res = (inch + foot * inchesInFoot) * smInInch;
    printf("%df %di = %.2fsm", foot, inch, res);

    if (res >= smInMeter) {
        printf(" = %dm %dsm", (int)(res / smInMeter), (int)(res - (res / smInMeter) * smInMeter));
    }
    printf("\n");

    return 0;
}