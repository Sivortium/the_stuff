// work2_5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
char getCharBetween(char start, char end)
{
    return rand() % (end - start + 1) + start;
}
void putChar(char*str, char h)
{
    //this function sets a random position in a string to a simbol
    int len = 0;
    while (*(str + len))
        len++;
    if (len)
    {
        int pos = rand() % (len+1);

        for (int g = len; g > pos; g--)
        {
            str[g] = str[g - 1];
        }
        str[pos] = h;
    }
    else
        str[0] = h;
}
int main()
{
    const int LENGTH = 8;//passwords length
    const int TOTAL = 10;//number of passwords
    srand(time(NULL));

    for (int j = 0; j < TOTAL; j++)
    {
        int len = LENGTH;
        char password[LENGTH+1] = { 0 };
        
        //random [1,len-2] - al least 1 of each is nedeed
        int ind = rand() % (len - 2) + 1;

        for (int i = 0; i < ind; i++)
        {
            putChar(password, getCharBetween('a','z'));
        }
        len -= ind;
        ind = rand() % (len - 1) + 1;

        for (int i = 0; i < ind; i++)
        {
            putChar(password, getCharBetween('A', 'Z'));
        }
        len -= ind;
        ind = len;

        for (int i = 0; i < ind; i++)
        {
            putChar(password, getCharBetween('0', '9'));
        }
        printf("%s\n",password);
    }
    printf("\n");
    return 0;
}