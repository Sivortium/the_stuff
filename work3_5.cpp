// work3_5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main()
{
    //array's limits
    const int MAX_VALUE = 100;
    const int MIN_VALUE = -100;

    unsigned int n;
    printf("Input the N - array's length\n");

    while (!scanf("%d", &n) || n < 2){
        fflush(stdin);
        printf("N needs to be an integer and N>1\n");
    }
    int *arr = NULL;
    arr = (int*)malloc(n*sizeof(int));
    srand(time(NULL));
    int sum = 0;
    bool SUM_HAS_BEGAN = false;

    //count limits
    int from = -1;
    int to = -1;

    for (int i = 0; i < n; i++){
        int num = rand() % (MAX_VALUE - MIN_VALUE + 1) + MIN_VALUE;
        arr[i] = num;
        printf("%d> %d\n", i, num);
        if (num < 0 && !SUM_HAS_BEGAN){
            SUM_HAS_BEGAN = true;
            from = i;
        }
        if (num > 0 && SUM_HAS_BEGAN){
            to = i;
        }
    }
    if (to >= 0 && from >= 0){
        sum = 0;
        for (int i = from; i <= to; i++){
            sum = sum + arr[i];
        }
        printf("The sum from %d to %d is:\n%d\n", arr[from], arr[to], sum);
    }
    else if (from == -1){
        printf("there is no negative number here, nothing to count\n");
    }
    else{
        printf("there is no positive number here, nothing to count\n");
    }
    free(arr);
    return 0;
}