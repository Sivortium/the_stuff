// work6_2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

const unsigned int MAX_VALUE = 1000000;
const unsigned int MIN_VALUE = 2;
//an array used to reduce iterations, each int is a number of iterations for N
//it acts like a cache
int cutted[MAX_VALUE - MIN_VALUE + 1] = { 0 };
void Kolats(unsigned int N, int *writeTo)
{
    if (N <= MAX_VALUE && N >= MIN_VALUE)
    {
        //if this number already was, just increment its number
        if (cutted[N - MIN_VALUE])
        {
            (*writeTo) += 1 + cutted[N - MIN_VALUE];
            return;
        }
    }
    if (N != 1)
    {
        (*writeTo)++;
        if (N % 2)
            Kolats(3 * N + 1, writeTo);
        else
            Kolats(N / 2, writeTo);
    }
}
int main()
{
    unsigned int ind = MIN_VALUE;
    int maxN = MIN_VALUE;
    int maxIterations = 0;
    Kolats(maxN, &maxIterations);
    while (ind <= MAX_VALUE)
    {
        if (!cutted[ind - MIN_VALUE])
        {
            int cur = 0;
            Kolats(ind, &cur);
            if (cur > maxIterations)
            {
                maxIterations = cur;
                maxN = ind;
            }
            cutted[ind - MIN_VALUE] = cur;
        }
        ind++;
    }
    printf("max N in [%d,%d] is %d, and it has %d iterations\n", MIN_VALUE, MAX_VALUE, maxN, maxIterations);
    return 0;
}