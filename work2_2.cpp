// work2_2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <time.h>
int PutNumber()
{
    int num=0;
    while (!scanf("%d",&num)){
        fflush(stdin);
        printf("Something wrong, use numbers only\n");
    }
    return num;
}
int _tmain(int argc, _TCHAR* argv[])
{
    srand(time(NULL));
    int tryes = 0;
    int theOne = rand() % 100 + 1;
    printf("Guess my number :D\nIt's somewhere there: [1;100]\n");
    int num;
    do {
        num = PutNumber();
        if (num > theOne){
            printf("Nope, it's less then that\n");
        } else if (num < theOne) {
            printf("Nope, it's more then that\n");
        }
        tryes++;
    } while (theOne != num);
    printf("Bingo! Got it from the %dth try\n", tryes);
    return 0;
}