// work5_1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//length of a line, also a maximum of words
const int LENGTH = 100;

void printWord(char *letter){
    //this function prints the letters until it is a ' ' or '\0' or '\n'(fgets)
    while (*letter && *letter != ' ' && *letter != '\n'){
        putchar(*letter++);
    }
}

int getWords(char *letter, char *arr[LENGTH]){
    //this function makes an array of pointers to the first letter of each word
    //and returns a number of words
    int words = 0;

    //checking the first simbol(nedeed for the "previous is space" check)
    if (*letter != ' '){
        arr[words] = letter;
        words++;
    }
    letter++;

    while (*letter){
        //if prevoius simbol is space and current is not
        if (*letter != ' ' && *(letter - 1) == ' '){
            arr[words] = letter;
            words++;
        }
        letter++;
    }
    return words;
}
int main()
{
    char *pointers[LENGTH];
    char line[LENGTH];

    printf("Type in your string: ");
    fgets(line, LENGTH, stdin);
    srand(time(NULL));
    printf("\nHere you go:\n\n");

    int words = getWords(line, pointers);

    for (int i = 0; i < words; i++){
        char *temp = pointers[i];
        //"i"th word is swapped to a randomly picked one in a half-interval [i; words)
        int changedTo = rand() % (words - i) + i;
        pointers[i] = pointers[changedTo];
        pointers[changedTo] = temp;
        printWord(pointers[i]);
        //if it is not the last word, place a space
        if (i < words - 1)
            putchar(' ');
    }
    putchar('\n');
    return 0;
}