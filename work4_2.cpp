// work3_1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#define LINE_LENGTH 200
int main()
{
    char line[200];
    printf("Input string to play with the words:\n");

    while (!gets(line)){
        fflush(stdin);
        printf("Something wrong, try again\n");
    }
    int ind = 1;
    char *words[200];
    int wordsInd = 0;
    if (line[0] != ' ')
        words[wordsInd++] = &line[0];

    while (line[ind]){
        //if it is a letter after ' ' OR before ' '
        //OR if it is the last letter
        if (line[ind] != ' ' && (line[ind - 1] == ' ' || line[ind + 1] == ' ') || !line[ind + 1])
            words[wordsInd++] = &line[ind];
        ind++;
    }
    wordsInd--;
    //here we got wordsInd about W*2-1, where W is a number of words
    for (; wordsInd > 0; wordsInd -= 2){
        //output the simbols between two pointers
        int move = words[wordsInd] - words[wordsInd - 1];
        while (move >= 0)
            putchar(*(words[wordsInd] - move--));
        //add a space before the next word, if it is
        if (wordsInd - 2)
            putchar(' ');
    }

    return 0;
}