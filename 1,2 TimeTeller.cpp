// TimeTeller.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
int main()
{
    int h;
    int m;
    int s;
    printf("What time is it?\n");

    while (scanf("%2d %2d %2d", &h, &m, &s) != 3){
        printf("You need to input numbers, try again\n");
        fflush(stdin);
    }

    while (h > 23 || h < 0){
        printf("Wow, wrong hours, must be positive and less then 24, not %d\n", h);
        fflush(stdin);
        check = scanf("%2d", &h);
    }

    while (m > 59 || m < 0){
        printf("Wow, wrong minutes, must be positive and less then 60, not %d\n", m);
        fflush(stdin);
        check = scanf("%2d", &m);
    }

    while (s > 59 || s < 0){
        printf("Wow, wrong seconds, must be positive and less then 60, not %d\n", s);
        fflush(stdin);
        check = scanf("%2d", &s);
    }

    if (h >= 6 && h < 10) {
        printf("Good morning! Take a cup of coffie and have a nice day!");
    } else if (h >= 10 && h < 12) {
        printf("It is still morning. =)");
    } else if (h >= 12 && h < 13) {
        printf("It's dinner time! Have a good meal!");
    } else if (h >= 13 && h < 17) {
        printf("Workin... Do some stuff.. Cause it's an afternoon.");
    } else if (h >= 17 && h < 19) {
        printf("Yey! You can go home now. =) Don't waste your time.");
    } else if (h >= 19 && h < 22) {
        printf("It is not so late, you can still watch some movies");
    } else if (h >= 22 && h < 23) {
        printf("Go to sleep, dude, it is too late.");
    } else {
        printf("Hey, what are you doing? Go to sleep, now!");
    }
    printf("\n");

    return 0;
}