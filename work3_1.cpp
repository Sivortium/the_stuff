// work3_1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
int main()
{
    char line[200];
    printf("Input string to count the words:\n");
    while (!gets(line)){
        fflush(stdin);
        printf("Something wrong, try again\n");
    }
    int ind = 1;
    int words = 0;
    if (line[0] != ' ')
        words++;
    while (line[ind])
    {
        if (line[ind] != ' ' && line[ind - 1] == ' ')
            words++;
        ind++;
    }
    printf("The line has %d words\n", words);
    return 0;
}