// work6_1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>



const int CONSOLE_WIDTH = 80;
const int LIST_WIDTH = 1;
int maxWidth = 1;
int maxPow = 1;


void fullFill(bool*root, int power){
    if (power){
        //width of the list, the bigger the list, the bigger the width
        /*
        for power=1 its just 1
        for power=2 is twice a number of (listWidth) +1:
        width is number of '*' including the '2'th
        *     *
        *    *2*
        *     *
        *  *  *  *
        * *2**1**2*
        *  *  *  *
        *     *
        *    *2*
        *     *
        for power=3(horizontal fragment):
        *     *        *        *
        *    ***      ***      ***
        *     *        *        *
        *  *  *  *  *  *  *  *  *  *
        * ****2********1********2****
        *  *  *  *  *  *  *  *  *  *
        *     *        *        *
        *    ***      ***      ***
        *     *        *        *

        etc..
        */

        //target use this width to draw a fractal with a decremented power
        int target = 1;
        for (int g = 1; g < power; g++){
            target *= 2 * LIST_WIDTH + 1;
        }
        //to top
        //if the listWidth is more then 1 this will fullFill draw the list
        int i;
        for (i = 1; i <= LIST_WIDTH; i++){
            *(root + i*maxWidth) = true;
            *(root - i*maxWidth) = true;
            *(root - i) = true;
            *(root + i) = true;
        }
        fullFill(root + target*maxWidth, power - 1);
        //to bottom
        fullFill(root - target*maxWidth, power - 1);
        //to left
        fullFill(root - target, power - 1);
        //to right
        fullFill(root + target, power - 1);
        //and the same point again
        fullFill(root, power - 1);
    }
    //when the function reach this placec it means the power to be 0
    //or when the fullfiling is over
    //at any way we put the simbol, we fullfill the array
    *(root) = true;
}

void printFractal(bool*Fr, int currentPower){
    for (int y = 0; y < maxWidth; y++){
        printf("%*c", (CONSOLE_WIDTH - maxWidth) / 2 - 1, ' ');
        for (int x = 0; x < maxWidth; x++){
            if (*(Fr + x + y*maxWidth) == true)
                putchar('*');
            else
                putchar(' ');
        }
        putchar('\n');
    }
}

int main(){
    maxPow = 0;
    while (maxWidth*(2 * LIST_WIDTH + 1) < CONSOLE_WIDTH){
        maxPow++;
        maxWidth *= 2 * LIST_WIDTH + 1;
    }
    bool *Fr = (bool*)malloc(sizeof(bool)*maxWidth*maxWidth);
    for (int e = 1; e <= maxPow; e++){
        fullFill(Fr + maxWidth / 2 + (maxWidth / 2)*maxWidth, e);
        printFractal(Fr, e);
        if (e == maxPow)
            printf("warning: this is the last fractal\n");
        system("pause");
        system("cls");
    }
    printf("thats it :) your console can provide only the %dth grade\n", maxPow);
    return 0;
}