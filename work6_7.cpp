// work6_7.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
//dimentions of a labirinth, h for height, w for width
const int LAB_W = 29;
const int LAB_H = 9;
const int SPEED = 500;
char LAB[LAB_H][LAB_W] = { "############################", "#           #   #          #", "##########  #   #          #", "#           #   #######  ###", "# ######    # x            #", "#      #    #   #######   ##", "#####  #    #   #         ##", "       #        #     ######", "############################" };

//starting position and the exit
char *position;
char *exitC;

void drawLabirinth(char *posit){
    //this function draws a labirinth
    system("cls");
    for (int h = 0; h < LAB_H; h++){
        for (int w = 0; w < LAB_W; w++){
            if (&LAB[h][w] == posit){
                putchar('x');
            }
            else{
                putchar(LAB[h][w]);
            }
        }
        putchar('\n');
    }
    Sleep(SPEED);
}
void findPath(char *(Way[LAB_H*LAB_W])[5]){
    //Way[i][0] - position
    //Way[i][1]-Way[i][4] - ways: top,right,bottom,left

    //if you wanna see the algorithm, uncoment lines with the output
    //and comment drawLabirinth function
    //printf("current position is x=%d,y=%d\n began looking\n", (position - &LAB[0][0]) % LAB_W, ((int)(position - &LAB[0][0]) - ((position - &LAB[0][0]) % LAB_W)) / LAB_W);
    int i = 1;
    while (Way[i][0]){
        int L = Way[i][0] - Way[i - 1][0];
        if (L < 0)
            L = -L;
        i++;
    }
    i--;
    //left, right, top and bottom
    char *tempPos[4];
    for (int o = 0; o < 4; o++){
        if (!Way[i][o + 1]){
            tempPos[o] = Way[i][0];
            while (true){
                if (!o)
                    tempPos[o] -= LAB_W;
                else if (o == 1)
                    tempPos[o]++;
                else if (o == 2)
                    tempPos[o] += LAB_W;
                else
                    tempPos[o]--;
                if (tempPos[o] == exitC)
                    break;
                if (*(tempPos[o] + 1) != '#' && o != 1 || *(tempPos[o] - 1) != '#' && o != 3 || *(tempPos[o] - LAB_W) != '#' && o != 0 || *(tempPos[o] + LAB_W) != '#' && o != 2){
                    //there are new ways
                    //printf("found a way: %d\n", o);
                    //here we need to check if this way already was(wide tonnels may make this programm running in circles)
                    bool WAS = false;
                    int ii = i;
                    while (ii >= 0){
                        if (Way[ii--][0] == tempPos[o])
                            WAS = true;
                    }
                    if (!WAS)
                        break;
                    else{
                        //printf("Ups, you already were here!");
                        Way[i][o + 1] = position;
                    }
                }
                if ((*(tempPos[o] + 1) == '#' || o == 1) && (*(tempPos[o] - 1) == '#' || o == 3) && (*(tempPos[o] - LAB_W) == '#' || o == 0) && (*(tempPos[o] + LAB_W) == '#' || o == 2)){
                    //there are no ways
                    //printf("this way is blocked: %d\n", o);
                    Way[i][o + 1] = position;
                    break;
                }
            }
        }
    }

    //found another turn or something
    //choose the way now
    int proxL = LAB_W*LAB_H*LAB_W*LAB_H;
    int proxE = -1;
    //find the closest point
    //exit:
    //XE: (exitC-LAB) % LAB_W
    //YE: ((exitC-LAB) - ((exitC-LAB) % LAB_W))/LAB_H
    //point:
    //XP: tempPos[e]-LAB % LAB_W
    //YP: (tempPos[e]-LAB - (tempPos[e]-LAB % LAB_W))/LAB_H
    //need minimum of that:
    //L=(XE-XP)*(XE-XP)+(YE-YP)*(YE-YP)
    for (int e = 0; e<4; e++){
        if (!Way[i][e + 1]){
            int tempV =
                (((exitC - &LAB[0][0]) % LAB_W) - ((&(*(tempPos[e])) - &LAB[0][0]) % LAB_W))
                * (((exitC - &LAB[0][0]) % LAB_W) - ((&(*(tempPos[e])) - &LAB[0][0]) % LAB_W))

                +

                ((((exitC - &LAB[0][0]) - ((exitC - &LAB[0][0]) % LAB_W)) / LAB_H) - ((tempPos[e] - &LAB[0][0] - ((tempPos[e] - &LAB[0][0]) % LAB_W)) / LAB_H))
                * ((((exitC - &LAB[0][0]) - ((exitC - &LAB[0][0]) % LAB_W)) / LAB_H) - ((tempPos[e] - &LAB[0][0] - ((&(*(tempPos[e])) - &LAB[0][0]) % LAB_W)) / LAB_H));
            if (proxL>tempV){
                proxL = tempV;
                proxE = e;
            }
        }
    }
    if (proxE == -1){
        //no ways, get back
        //printf("no ways, I'm getting back");
        int tempW = Way[i - 1][0] - Way[i][0];
        if (tempW < 0){
            if (-tempW < LAB_W){
                //came from top, so its forbitten to go down again
                Way[i - 1][2] = Way[i][0];
            }
            else{
                //from left
                Way[i - 1][1] = Way[i][0];
            }
        }
        else{
            if (tempW < LAB_W){
                //from bottom
                Way[i - 1][0] = Way[i][0];
            }
            else{
                //from right
                Way[i - 1][4] = Way[i][0];
            }
        }
        Way[i][0] = 0;
        Way[i][1] = 0;
        Way[i][2] = 0;
        Way[i][3] = 0;
        Way[i][4] = 0;
        findPath(Way);
    }
    else{
        if (tempPos[proxE] == exitC){
            position = exitC;
            drawLabirinth(position);
            printf("Yeah! I came.\n");
        }
        else{
            Way[i][proxE] = position;
            i++;
            //block the way, from what we came: 1->3 2->4 3>1 4>2
            Way[i][(proxE + 2) % 4 + 1] = position;

            Way[i][0] = tempPos[proxE];
            position = Way[i][0];
            if (*(position - LAB_W) == '#')
            {
                Way[i][1] = position - LAB_W;
            }
            if (*(position + 1) == '#')
            {
                Way[i][2] = position + 1;
            }
            if (*(position + LAB_W) == '#')
            {
                Way[i][3] = position + LAB_W;
            }
            if (*(position - 1) == '#')
            {
                Way[i][4] = position - 1;
            }
            //printf("\n");
            drawLabirinth(position);
            findPath(Way);
        }
    }
}

int main()
{
    char *(WAY[LAB_W*LAB_H])[5] = { 0 };
    for (int h = 0; h < LAB_H; h++){
        for (int w = 0; w < LAB_W; w++){
            if (LAB[h][w] == 'x' || LAB[h][w] == 'X'){
                LAB[h][w] = ' ';
                //point the starting position
                position = &LAB[h][w];
            }
            if (LAB[h][w] == ' ' && (h == 0 || w == 0 || h == LAB_H - 1 || w == LAB_W - 1)){
                //point the exit
                exitC = &LAB[h][w];
            }
        }
    }
    WAY[0][0] = position;
    if (*(position - LAB_W) == '#')
    {
        WAY[0][1] = position - LAB_W;
    }
    if (*(position + 1) == '#')
    {
        WAY[0][2] = position + 1;
    }
    if (*(position + LAB_W) == '#')
    {
        WAY[0][3] = position + LAB_W;
    }
    if (*(position - 1) == '#')
    {
        WAY[0][4] = position - 1;
    }
    findPath(WAY);
    return 0;
}