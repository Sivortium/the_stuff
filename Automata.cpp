// Automata.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <conio.h>
#include <windows.h>
#include <string.h>
#include <ctime>
#define CONSOLE_WIDTH 81
#define Copyright "Console market. Pick some and give me all your money"

class DrinkDealer{
public:
    DrinkDealer(const unsigned int _context_width = MinBarWidth){
        Width = _context_width;
        if (_context_width < MinBarWidth)
        {
            Width = MinBarWidth;
        }
    };
    void prepareDrink(int _preparingID, int _cookFrames){
        cookFrames = _cookFrames;
        status = PREPARING_DRINK;
        startedFrame = 0;
        preparingID = _preparingID;
    }
    int iteratePreparation(int _frame){
        if (!startedFrame)
            startedFrame = _frame;
        _frame -= startedFrame;
        int persantage = _frame;
        if (persantage >= cookFrames)
        {
            status = WAITING;
            persantage = 100;
        }
        else
            persantage = persantage * 100 / cookFrames;
        return persantage;
    }

    enum states{ OFF, PREPARING_DRINK, WAITING };
    int status = OFF;
    int preparingID;
    int Width;
    static const int GlassInnerWidth = 3;
    static const int GlassInnerHeight = 3;
private:
    int cookFrames;
    int startedFrame = 0;
    static const int MinBarWidth = 11;
};
class CashCatcher{
public:
    CashCatcher(const char *TXT_PATH = "money.txt"){
        FILE *F;
        dbPath = TXT_PATH;
        if (!(F = fopen(dbPath, "at+"))){
            printf("Error: cannot create a database for money, continue?");
            _getch();
        }
        int BufferLength = 10;
        totalCoins = 0;
        char *buf = (char*)malloc(BufferLength*sizeof(char));
        while (fgets(buf, BufferLength, F))
            totalCoins++;

        if (totalCoins){
            money = new int *[totalCoins];
            rewind(F);
            int p = 0;
            while (fgets(buf, BufferLength, F))
            {
                char *_coin = (char*)malloc(BufferLength*sizeof(char));
                char *_number = (char*)malloc(BufferLength*sizeof(char));
                int i = 0;
                int j = 0;
                while (buf[i] != '/' && buf[i] && buf[i] != '\n')
                    _coin[j++] = buf[i++];
                _coin[j] = 0;
                j = 0;
                i++;
                while (buf[i] != '/' && buf[i] && buf[i] != '\n')
                    _number[j++] = buf[i++];
                _number[j] = 0;
                if (strlen(_number) && strlen(_coin)){
                    money[p] = new int[2];
                    money[p][0] = atoi(_coin);
                    money[p++][1] = atoi(_number);
                }
                free(_coin);
                free(_number);
            }
            totalCoins = p;
            for (int h = 0; h < totalCoins; h++)
            {
                int max = h;
                for (int w = h + 1; w < totalCoins; w++)
                {
                    if (money[w][0]>money[max][0])
                        max = w;
                }
                int temp = money[max][0];
                money[max][0] = money[h][0];
                money[h][0] = temp;
                temp = money[max][1];
                money[max][1] = money[h][1];
                money[h][1] = temp;
            }
            const short maxCharsForState = 5;
            const short totalStates = 3;
            displayStates = (char **)malloc(sizeof(char*)*maxCharsForState*totalStates);
            displayStates[0] = " <-> ";
            displayStates[1] = "[<->]";
            displayStates[2] = "[< >]";
            const short maxCharsForReturnState = 5;
            const short totalReturnStates = 2;
            displayReturnStates = new char*[totalReturnStates];
            displayReturnStates[0] = new char[maxCharsForReturnState];
            displayReturnStates[0] = " (r) ";
            displayReturnStates[1] = new char[maxCharsForReturnState];
            displayReturnStates[1] = "[(r)]";
        }
        else{
            //in case the Automata does not have any coins, here we tell what coins it can accept
            //100,50,10,5,2,1 => 6
            totalCoins = 6;
            money = new int*[totalCoins];
            for (int q = 0; q < totalCoins; q++){
                money[q] = new int[2];
                money[q][1] = 0;
            }
            money[0][0] = 100;
            money[1][0] = 50;
            money[2][0] = 10;
            money[3][0] = 5;
            money[4][0] = 2;
            money[5][0] = 1;
            refreshBase();
        }
        fclose(F);
    };
    bool checkForChange(int value){
        for (int i = 0; i <totalCoins && value; i++)
        {
            if (value >= money[i][0] && money[i][1])
            {
                int down = value / money[i][0];
                if (down > money[i][1])
                    down = money[i][1];
                value -= down*money[i][0];
            }
        }
        return !value;
    }
    int** giveMoney(int value){
        int ** localCoins = new int*[totalCoins];
        for (int i = 0; i < totalCoins; i++)
        {
            localCoins[i] = new int[2];
            localCoins[i][0] = money[i][0];
            localCoins[i][1] = 0;
            if (value >= money[i][0] && money[i][1]){
                int down = value / money[i][0];
                if (down > money[i][1])
                    down = money[i][1];
                value -= down*money[i][0];
                money[i][1] -= down;
                localCoins[i][1] = down;
            }
        }
        refreshBase();
        return localCoins;
    }
    void refreshBase(){
        FILE *F;
        bool Base_connected = true;
        if (!(F = fopen(dbPath, "wt"))){
            return;
        }
        for (int d = 0; d < totalCoins; d++)
        {
            if (d)
                fputc('\n', F);
            fprintf(F, "%d/%d", money[d][0], money[d][1]);
        }
        fclose(F);
    }
    int takeMoney(){
        int rets = 0;
        const int localBuff = 5;
        char * buf = new char[localBuff];
        fgets(buf, localBuff, stdin);
        rets = atoi(buf);
        for (int p = 0; p < totalCoins; p++){
            if (money[p][0] == rets){
                money[p][1]++;
                refreshBase();
                return rets;
            }
        }
        if (rets)
            status = UNEXEPTABLE_COIN;
        return 0;
    }
    ~CashCatcher(){
        free(displayStates);
    }
    int totalCoins = 0;
    int **money;
    enum states{ OFF, HOVER, SELECTED, HOVER_RETURN, UNEXEPTABLE_COIN };
    int status = OFF;
    char ** displayStates;
    char ** displayReturnStates;
private:
    const char * dbPath;
};
class MessageDisplay{
public:
    MessageDisplay(const unsigned int _width) :Width(_width){
        if (Width < Min_display_width)
        {
            printf("Error: Display width is too small, need at least %d simbols\n", Min_display_width);
            _getch();
            exit(4);
        }
        cuttedMsg = (char*)malloc(sizeof(char)*Width);
    }
    ~MessageDisplay(){
        //free(cuttedMsg);
        //delete[] nextMsg;
        //delete[] msg;
    }
    char * getMsg(unsigned long _frame){
        if (!msg) return 0;
        if (!starterFrame)
        {
            starterFrame = _frame;
        }
        int length = strlen(msg);
        /*check first*/
        int totalFrames;
        int dash = _frame - starterFrame;
        switch (curAnimationType){
        case INSTANT:
            totalFrames = FramesForSimbol*length;
            if (totalFrames < MinFramesForInstantType)
                totalFrames = MinFramesForInstantType;
            break;
        case INSTANT_CONSTANT:
            totalFrames = dash + 1;
            break;
        case INSTANT_BLINK:
            totalFrames = FramesForSimbol*length;
            if (totalFrames<MinFramesForInstantType)
                totalFrames = MinFramesForInstantType;
            break;
        case INSTANT_MOVE:
            totalFrames = length - Width + LeftStopForInstantMoveType + RightStopForInstantMoveType;
            break;
        case SLOW_MOVE:
            totalFrames = length + Width;
            break;
        }
        if (dash > totalFrames){
            if (!nextMsg && defaultMsg){
                nextMsg = defaultMsg;
                nextAnimationType = INSTANT;
                if (strlen(nextMsg) >= Width)
                    nextAnimationType = INSTANT_MOVE;
            }
            else
                return 0;
            msg = nextMsg;
            length = strlen(msg);
            starterFrame = _frame;
            curAnimationType = nextAnimationType;
            nextMsg = NULL;
        }
        //fill with the spaces
        for (int t = 0; t < Width; t++)
            cuttedMsg[t] = ' ';
        if (cuttedMsg[Width]) cuttedMsg[Width] = 0;
        switch (curAnimationType){
        case INSTANT:
            dash = _frame - starterFrame;
            totalFrames = FramesForSimbol*length;
            if (totalFrames < MinFramesForInstantType)
                totalFrames = MinFramesForInstantType;
            for (int g = 0; g < length; g++)
                cuttedMsg[(Width - length) / 2 + g] = msg[g];
            break;
        case INSTANT_CONSTANT:
            if (length < Width){
                for (int g = 0; g < length; g++)
                    cuttedMsg[(Width - length) / 2 + g] = msg[g];
            }
            else{
                dash = _frame - starterFrame;
                totalFrames = length - Width + LeftStopForInstantMoveType + RightStopForInstantMoveType;
                if (dash >= totalFrames){
                    starterFrame = _frame;
                    dash = 0;
                }
                if (dash < LeftStopForInstantMoveType)
                    dash = 0;
                else if (dash>totalFrames - RightStopForInstantMoveType)
                    dash = length - Width;
                else
                    dash -= LeftStopForInstantMoveType;
                for (int g = 0; g < Width; g++)
                    cuttedMsg[g] = msg[g + dash];
            }
            break;
        case INSTANT_BLINK:
            dash = _frame - starterFrame;
            if (dash % BlinkEachFrame>0)
            {
                for (int g = 0; g < length; g++)
                    cuttedMsg[(Width - length) / 2 + g] = msg[g];
            }
            break;
        case INSTANT_MOVE:
            dash = _frame - starterFrame;
            totalFrames = length - Width + LeftStopForInstantMoveType + RightStopForInstantMoveType;
            if (dash < LeftStopForInstantMoveType)
                dash = 0;
            else if (dash>totalFrames - RightStopForInstantMoveType)
                dash = length - Width;
            else
                dash -= LeftStopForInstantMoveType;
            for (int g = 0; g < Width; g++)
                cuttedMsg[g] = msg[g + dash];
            break;
        case SLOW_MOVE:
            dash = _frame - starterFrame;

            if (dash<Width && dash>length){
                for (int g = 0; g < length; g++)
                    cuttedMsg[Width - dash + g] = msg[g];
            }
            else if (dash < Width){
                for (int g = Width - dash; g < Width; g++)
                    cuttedMsg[g] = msg[g + dash - Width];
            }
            else{
                for (int g = 0; g < Width && g < Width - dash + length; g++)
                    cuttedMsg[g] = msg[g + dash - Width];
            }
            break;
        }
        return cuttedMsg;
    }
    void pushMsg(const char * _msg, const int _animationType = INSTANT, bool _FORCE = false){
        if (!_FORCE && msg){
            nextMsg = _msg;
            nextAnimationType = _animationType;
            if (_animationType == INSTANT && strlen(_msg) >= Width)
                nextAnimationType = INSTANT_MOVE;
            else if (_animationType == INSTANT_MOVE && strlen(_msg) < Width)
                nextAnimationType = INSTANT;
        }
        else{
            msg = _msg;
            nextMsg = NULL;
            starterFrame = 0;
            curAnimationType = _animationType;
            if (_animationType == INSTANT && strlen(_msg) >= Width)
                curAnimationType = INSTANT_MOVE;
            else if (_animationType == INSTANT_MOVE && strlen(_msg) < Width)
                curAnimationType = INSTANT;
        }
    }
    void setDefaultMessage(const char * _msg){
        defaultMsg = _msg;
    }
    const int Width;
    enum AnimationType{ SLOW_MOVE, INSTANT, INSTANT_MOVE, INSTANT_BLINK, INSTANT_CONSTANT };
private:
    const int Min_display_width = 3;

    const char * msg;
    char * cuttedMsg;
    const char * defaultMsg;
    const char * nextMsg;
    int curAnimationType;
    int nextAnimationType;
    const int MinFramesForInstantType = 20;
    const int BlinkEachFrame = 7;
    const int MinFramesForInstantBlinkType = 20;
    const int LeftStopForInstantMoveType = 5;
    const int RightStopForInstantMoveType = 10;
    const int FramesForSimbol = 1;
    unsigned int starterFrame;
};
class Drink{
public:
    Drink(const char*_name, const short _price, const int _amount, const short _preparationTime) :name(_name), price(_price), amount(_amount), preparationTime(_preparationTime){}
    Drink operator= (Drink& dr){
        (*this).name = dr.name;
        (*this).price = dr.price;
        (*this).amount = dr.amount;
        (*this).preparationTime = dr.preparationTime;
        return *this;
    }
    const char * name;
    short price;
    int amount;
    short preparationTime;
};
class DrinkBase{
public:
    DrinkBase(const unsigned int _context_width, const char *TXT_PATH = "drinks.txt") :ContextWidth(_context_width){
        maxNameLen = _context_width / columns - 1 - columns; //1 for left space and %d for each right space
        FILE *F;
        status = E_BASE_NOT_FOUND;
        dbPath = TXT_PATH;
        if (!(F = fopen(dbPath, "rt+"))){
            printf("Error: drink's base not found");
            _getch();
            return;
        }
        status = E_ZERO_DRINKS;
        char *buf = (char*)malloc(BufferLength*sizeof(char));
        while (fgets(buf, BufferLength, F))
            TotalDrinks++;

        if (TotalDrinks){
            drinks = (Drink *)malloc(TotalDrinks*sizeof(Drink));
            rewind(F);
            int p = 0;
            int maxColuwmWidth = 0;
            while (fgets(buf, BufferLength, F))
            {
                char *_name = (char*)malloc(maxNameLen*sizeof(char));
                char *_price = (char*)malloc(BufferLength*sizeof(char));
                char *_number = (char*)malloc(BufferLength*sizeof(char));
                char *_time = (char*)malloc(BufferLength*sizeof(char));
                int i = 0;
                int j = 0;
                while (buf[i] != '/' && buf[i] && buf[i] != '\n')
                    _name[j++] = buf[i++];
                _name[j] = 0;
                if (maxColuwmWidth < j)
                    maxColuwmWidth = j;
                j = 0;
                i++;
                while (buf[i] != '/' && buf[i] && buf[i] != '\n')
                    _price[j++] = buf[i++];
                _price[j] = 0;
                j = 0;
                i++;
                while (buf[i] != '/' && buf[i] && buf[i] != '\n')
                    _number[j++] = buf[i++];
                _number[j] = 0;
                j = 0;
                i++;
                while (buf[i] != '/' && buf[i] && buf[i] != '\n')
                    _time[j++] = buf[i++];
                _time[j] = 0;
                if (strlen(_name) && strlen(_price) && strlen(_number) && strlen(_time))
                    drinks[p++] = Drink(_name, atoi(_price), atoi(_number), atoi(_time));
                free(_price);
                free(_number);
                free(_time);
            }
            TotalDrinks = p;
            if (maxColuwmWidth < maxNameLen)
                maxNameLen = maxColuwmWidth;
            maxColuwmWidth += strlen(commonState);
            columns = ContextWidth / maxColuwmWidth;
            if (columns>1){
                extraSideSpace = ((ContextWidth - maxColuwmWidth*columns) % (columns - 1)) / 2;
                betweenSpace = (ContextWidth - maxColuwmWidth*columns) / (columns - 1);
            }
            else
                extraSideSpace = (ContextWidth - maxColuwmWidth) / 2;

            status = SUCCESSFUL_INIT;
        }
        fclose(F);
    }
    char * getInfo(const int id){
        char * msg = new char[BufferLength];
        sprintf(msg, "%s - %d coins", drinks[id].name, drinks[id].price);
        return msg;
    }
    void refreshBase(){
        FILE *F;
        bool Base_connected = true;
        if (!(F = fopen(dbPath, "wt"))){
            Base_connected = false;
        }
        for (int d = 0; d < TotalDrinks; d++)
        {
            if (!drinks[d].amount)
            {
                for (int r = d; r < TotalDrinks - 1; r++)
                {
                    drinks[r] = drinks[r + 1];
                }
                TotalDrinks--;
            }
            if (Base_connected){
                if (d)
                    fputc('\n', F);
                fprintf(F, "%s/%d/%d/%d", drinks[d].name, drinks[d].price, drinks[d].amount, drinks[d].preparationTime);
            }
        }
        fclose(F);
    }
    ~DrinkBase(){
        delete[] drinks;
    }
    unsigned int TotalDrinks = 0;
    const int ContextWidth;
    const char * commonState = " o  ";
    const char * hoverState = "[o] ";
    Drink * drinks;
    int columns = 1;
    int betweenSpace = 1;
    int extraSideSpace = 0;
    int status;
    enum states{ E_BASE_NOT_FOUND, E_ZERO_DRINKS, SUCCESSFUL_INIT };
private:
    int maxNameLen;
    const int BufferLength = 200;
    const char * dbPath;
};
class Automata{
public:
    int status;
    Automata(unsigned int _redraw_delay, unsigned int _width = CONSOLE_WIDTH*0.75, unsigned int _statusMsgBar_width = CONSOLE_WIDTH*0.55, unsigned int _drinksBar_width = CONSOLE_WIDTH*0.5) :Redraw_delay(_redraw_delay), Width(_width), msgBar(_statusMsgBar_width), drinks(_drinksBar_width){
        IS_ON = false;
        if (Width<Min_automata_width)
        {
            printf("Error: Automata width is too small, use at least %d simbols\n", Min_automata_width);
            _getch();
            status = E_SMALL_WIDTH;
            exit(E_SMALL_WIDTH);
        }
        else if (Width>CONSOLE_WIDTH)
        {
            printf("Error: Automata width is too big, maximum supported value is %d simbols\n", CONSOLE_WIDTH);
            _getch();
            status = E_HUGE_WIDTH;
            exit(E_HUGE_WIDTH);
        }

        if (_statusMsgBar_width >= Width - 2)
        {
            printf("Error: Message bar width is too big, should be less than Automata width (keep in mind that 2 simbols are for side borders\n");
            _getch();
            status = E_HUGE_MSGBAR;
            exit(E_HUGE_MSGBAR);
        }
        msgBar.pushMsg("Hi there, that's my first big shot in C++ classes. Use arrow keys to move around and Enter to choose the stuff.", msgBar.SLOW_MOVE);
        status = E_DRINKS_BASE_ERROR;
        if (drinks.status == drinks.E_BASE_NOT_FOUND)
            msgBar.pushMsg("Drinks database was not found, check the preferences and paths", msgBar.SLOW_MOVE, true);
        else if (drinks.status == drinks.E_ZERO_DRINKS)
            msgBar.pushMsg("Drinks list is empty, nothing to sell, come back later :)", msgBar.SLOW_MOVE, true);
        else if (drinks.status == drinks.SUCCESSFUL_INIT)
            status = SUCCESSFUL_INIT;
        else
            status = E_UNKNOWN;
        Msg_PushingReturnFailed = new const char*[totalFailReturnMessages];
        Msg_PushingReturnFailed[0] = "May it be the button stucked, of you just pushed it badly, but you get nothing";
        Msg_PushingReturnFailed[1] = "You can swear that you heard the sound of coins, but you can't see them";
        Msg_PushingReturnFailed[2] = "The machine responds with the absolute silence to you";
    }
    void redrawState(){
        system("cls");
        redrawFrame++;
        unsigned int leftSpace = CONSOLE_WIDTH / 2 - Width / 2;
        printf("%*c", leftSpace, Top_char);
        for (unsigned int g = 0; g < Width; g++){
            putchar(Top_char);
        }
        printf("\n%*c%*c", leftSpace, Side_char, Width, Side_char);
        //print name
        printf("\n%*c%*s%*c\n%*c%*c", leftSpace, Side_char, (Width + strlen(Copyright)) / 2, Copyright, Width - (Width + strlen(Copyright)) / 2, Side_char, leftSpace, Side_char, Width, Side_char);
        //draw status bar
        char * displayedMsg = msgBar.getMsg(redrawFrame);
        int leftBarSpace = (Width - msgBar.Width - 1) / 2;
        int rightBarSpace = Width - leftBarSpace - msgBar.Width - 1;
        printf("\n%*c%*c", leftSpace, Side_char, leftBarSpace, Top_char);
        for (unsigned int g = 0; g <= msgBar.Width; g++){
            putchar(Top_char);
        }
        printf("%*c\n%*c%*c", rightBarSpace, Side_char, leftSpace, Side_char, leftBarSpace, Side_char);
        if (displayedMsg)
            printf("%s", displayedMsg);
        else{
            printf("%*c", msgBar.Width, ' ');
        }
        printf("%c%*c", Side_char, rightBarSpace, Side_char);
        printf("\n%*c%*c", leftSpace, Side_char, leftBarSpace, Top_char);
        for (unsigned int g = 0; g <= msgBar.Width; g++){
            putchar(Top_char);
        }
        printf("%*c\n%*c%*c", rightBarSpace, Side_char, leftSpace, Side_char, Width, Side_char);
        /*draw cash bar*/
        char * cashState = cashTaker.displayStates[cashTaker.status <= cashTaker.SELECTED ? cashTaker.status : cashTaker.OFF];
        char * cashReturnState = cashTaker.displayReturnStates[cashTaker.status == cashTaker.HOVER_RETURN ? 1 : cashTaker.OFF];
        const int leftCashReturnStateSpace = 2;
        const int leftCashStateSpace = Width / 2 - (strlen(cashState) + strlen(cashReturnState) + leftCashReturnStateSpace) / 2 + strlen(cashState);
        printf("\n%*c%*s%*s%*c\n%*c%*c", leftSpace, Side_char, leftCashStateSpace, cashState, leftCashReturnStateSpace, cashReturnState, Width - 1 - leftCashStateSpace - leftCashReturnStateSpace - strlen(cashState) / 2, Side_char, leftSpace, Side_char, Width, Side_char);
        /*print drinks*/
        int i = 0;
        int columnWidth = drinks.ContextWidth - drinks.extraSideSpace * 2 - drinks.betweenSpace*(drinks.columns - 1);
        columnWidth /= drinks.columns;
        while (i < drinks.TotalDrinks){
            printf("\n%*c", leftSpace, Side_char);
            int sideSpace = drinks.extraSideSpace + (Width - drinks.ContextWidth) / 2;
            if (sideSpace)
                printf("%*c", sideSpace - 1, ' ');
            int nameLength;
            int c = 0;
            for (; c < drinks.columns && i < drinks.TotalDrinks; c++){
                if (c && columnWidth - nameLength + drinks.betweenSpace)
                    printf("%*c", columnWidth - nameLength + drinks.betweenSpace - 1, ' ');
                printf("%s%s", i == drinkSelected ? drinks.hoverState : drinks.commonState, drinks.drinks[i].name);
                nameLength = strlen(drinks.drinks[i].name) + strlen(i == drinkSelected ? drinks.hoverState : drinks.commonState);
                i++;
            }
            sideSpace = Width - (sideSpace + columnWidth*(c - 1) + nameLength + drinks.betweenSpace*(c - 1)) + c;
            printf("%*c", sideSpace, Side_char);
            printf("\n%*c%*c", leftSpace, Side_char, Width, Side_char);
        }
        /*print the outputer*/
        printf("%\n%*c%*c", leftSpace, Side_char, Width, Side_char);
        int outputterLeftSpace = (Width - outputer.Width - 1) / 2;
        int outputterRightSpace = Width - outputterLeftSpace - outputer.Width - 1;
        printf("\n%*c%*c", leftSpace, Side_char, outputterLeftSpace, Top_char);
        for (int g = 0; g <= outputer.Width; g++){
            putchar(Top_char);
        }
        printf("%*c\n%*c%*c%*c%*c", outputterRightSpace, Side_char, leftSpace, Side_char, outputterLeftSpace, Side_char, outputer.Width + 1, Side_char, outputterRightSpace, Side_char);
        if (outputer.status > outputer.OFF){
            int outputterGlassLeftSpace = (outputer.Width - outputer.GlassInnerWidth) / 2;
            int outputterGlassRightSpace = outputer.Width - outputer.GlassInnerWidth - outputterGlassLeftSpace;
            int process = outputer.iteratePreparation(redrawFrame);
            for (int line = outputer.GlassInnerHeight; line > 0; line--)
            {
                const char leftSider = status == OUTPUT_BLOCK_SELECTED&&line == outputer.GlassInnerHeight / 2 + 1 ? '>' : Side_char;
                const char rightSider = status == OUTPUT_BLOCK_SELECTED&&line == outputer.GlassInnerHeight / 2 + 1 ? '<' : Side_char;
                printf("\n%*c%*c%*c", leftSpace, Side_char, outputterLeftSpace, leftSider, outputterGlassLeftSpace, Side_char);
                if (100 / outputer.GlassInnerHeight*line <= process){
                    for (int water = 0; water < outputer.GlassInnerWidth; water++)
                        putchar('#');
                }
                else
                    printf("%*c", outputer.GlassInnerWidth, ' ');
                printf("%c%*c%*c", Side_char, outputterGlassRightSpace, rightSider, outputterRightSpace, Side_char);
            }
            printf("\n%*c%*c%*c", leftSpace, Side_char, outputterLeftSpace, Side_char, outputterGlassLeftSpace, Top_char);
            for (int g = 0; g <= outputer.GlassInnerWidth; g++){
                putchar(Top_char);
            }
            printf("%*c%*c", outputterGlassRightSpace, Side_char, outputterRightSpace, Side_char);
        }
        else{
            for (int s = 0; s < outputer.GlassInnerHeight / 2; s++)
                printf("\n%*c%*c%*c%*c", leftSpace, Side_char, outputterLeftSpace, Side_char, outputer.Width + 1, Side_char, outputterRightSpace, Side_char);
            const char leftSider = status == OUTPUT_BLOCK_SELECTED ? '>' : Side_char;
            const char rightSider = status == OUTPUT_BLOCK_SELECTED ? '<' : Side_char;
            printf("\n%*c%*c%*c%*c", leftSpace, Side_char, outputterLeftSpace, leftSider, outputer.Width + 1, rightSider, outputterRightSpace, Side_char);
            for (int s = outputer.GlassInnerHeight / 2 + 1; s < outputer.GlassInnerHeight + 1; s++)
                printf("\n%*c%*c%*c%*c", leftSpace, Side_char, outputterLeftSpace, Side_char, outputer.Width + 1, Side_char, outputterRightSpace, Side_char);
        }
        printf("\n%*c%*c", leftSpace, Side_char, outputterLeftSpace, Top_char);
        for (unsigned int g = 0; g <= outputer.Width; g++){
            putchar(Top_char);
        }
        printf("%*c", outputterRightSpace, Side_char);
        //print the bottom
        printf("\n%*c%*c", leftSpace, Side_char, Width, Side_char);

        printf("\n%*c", leftSpace, Top_char);
        for (unsigned int g = 0; g < Width; g++){
            putchar(Top_char);
        }
        putchar('\n');
    }
    void turnOn(){
        if (!IS_ON){
            IS_ON = true;
            watch();
        }
    }
    void turnOff(){
        if (IS_ON){
            IS_ON = false;
        }
    }
private:
    bool IS_ON;
    enum states{ E_SMALL_WIDTH, E_HUGE_WIDTH, E_HUGE_MSGBAR, E_DRINKS_BASE_ERROR, E_UNKNOWN, SUCCESSFUL_INIT, DRINK_BLOCK_SELECTED, CASH_BLOCK_SELECTED, OUTPUT_BLOCK_SELECTED, CASH_TAKING, CONFIRMING };
    const int Redraw_delay;
    unsigned long redrawFrame = 0;
    const unsigned int Width;
    const unsigned int Min_automata_width = 20;
    const char Side_char = '|';
    const char Top_char = '-';

    MessageDisplay msgBar;
    DrinkBase drinks;
    CashCatcher cashTaker;
    int deposited = 0;
    DrinkDealer outputer;

    enum Keys
    {
        TOP_KEY = 72,
        BOTTOM_KEY = 80,
        LEFT_KEY = 75,
        RIGHT_KEY = 77,
        ENTER_KEY = 13
    };
    const char* Msg_HoveringMoneyBlock = "This is the place where your money wanish";
    const char* Msg_HoveringMoneyReturnerWithEmptyDeposit = "This is a money return button";
    const char* Msg_HoveringMoneyReturner = "You may try to return your money... They say there is a %d chance to do it";
    const char* Msg_MoneyReturnRepeater = "%d of %ds";
    const char* Msg_MoneyReturnBeginText = "Automata returns you: ";
    const char* Msg_MoneyReturnSeparator = ", ";
    const char* Msg_StartCashRecieving = "Ok, give me your money";
    const char* Msg_NotEnoughMoney = "Sorry, I need %d more coins from you";
    const char* Msg_DepositStatus = "Current deposit is %d coins.";
    const char* Msg_CancelingMoneyInput = "Oh, whatever.. You will give it to me later";
    const char* Msg_UnexeptableCoin = "You just put some wierd coin, I won't take it";
    const char* Msg_PushingReturnWithoutDeposit = "Wow.. you need to put something here first";
    const char* Msg_DefaultDisplayDepositEmpty = "Put some coins so you can order a drink";
    const char* Msg_DefaultDisplayDeposit = "Deposited %d coins";
    const char** Msg_PushingReturnFailed;
    const int returnChance = 30;//%
    const int totalFailReturnMessages = 3;
    const char* Msg_DrinkIsStillPrepairing = "Your drink is not ready yet, wait";
    const char* Msg_TakingEmptyDrink = "There is nothing here, order the drink above first";
    const char* Msg_TakingDrink = "Have a nice drink and come back later";
    const char* Msg_OrderingWithoutTaking = "You've just ordered a drink, take it, drink and then order again";
    const char* Msg_ExactSumm = "You have so many different coins. Well, thanks for giving the exact summ";
    const char* Msg_ThanksForOrder = "Thanks for purchaising a drink";
    const char* Msg_OrderCanceled = "Order was canceled";
    const char* Msg_NoCoinsToReturn = "Ups.. I don't have enough coins to return the rest of the summ to you, continue?";
    const char* Msg_OotputBlockHovered = "Your drink will appear here";

    int drinkSelected = -1;
    void hoverDrink(){
        if (drinkSelected != -1)
            msgBar.pushMsg(drinks.getInfo(drinkSelected), msgBar.INSTANT_CONSTANT, true);
    }
    void watch(){
        srand(time(0));
        while (true)
        {
            while (!_kbhit()){
                redrawState();
                Sleep(Redraw_delay);
            }
            char p = _getch();
            int money = 0;
            char *buffForMsg = new char[strlen(Msg_DefaultDisplayDeposit) + 10];
            switch (status)
            {
            case DRINK_BLOCK_SELECTED:
                switch (p){
                case TOP_KEY:
                    if (drinkSelected >= drinks.columns)
                        drinkSelected -= drinks.columns;
                    else{
                        status = CASH_BLOCK_SELECTED;
                        drinkSelected = -1;
                        cashTaker.status = cashTaker.HOVER;
                        msgBar.pushMsg(Msg_HoveringMoneyBlock, msgBar.INSTANT, true);
                    }
                    hoverDrink();
                    break;
                case BOTTOM_KEY:
                    if (drinkSelected + drinks.columns < drinks.TotalDrinks)
                        drinkSelected += drinks.columns;
                    else{
                        status = OUTPUT_BLOCK_SELECTED;
                        drinkSelected = -1;
                        if (outputer.status == outputer.PREPARING_DRINK)
                            msgBar.pushMsg(Msg_DrinkIsStillPrepairing, msgBar.INSTANT, true);
                        else if (outputer.status == outputer.WAITING)
                            msgBar.pushMsg(Msg_TakingDrink, msgBar.INSTANT, true);
                        else
                            msgBar.pushMsg(Msg_OotputBlockHovered, msgBar.INSTANT, true);
                    }
                    hoverDrink();
                    break;
                case LEFT_KEY:
                    if (drinkSelected)
                        drinkSelected--;
                    hoverDrink();
                    break;
                case RIGHT_KEY:
                    if (drinkSelected < drinks.TotalDrinks - 1)
                        drinkSelected++;
                    hoverDrink();
                    break;
                case ENTER_KEY:
                    if (outputer.status == outputer.OFF){
                        Drink selectedDrinkData = drinks.drinks[drinkSelected];
                        if (deposited >= selectedDrinkData.price){
                            if (deposited == selectedDrinkData.price)
                                msgBar.pushMsg(Msg_ExactSumm, msgBar.INSTANT, true);
                            else if (cashTaker.checkForChange(deposited - selectedDrinkData.price))
                                msgBar.pushMsg(Msg_ThanksForOrder, msgBar.INSTANT);
                            else{
                                msgBar.pushMsg(Msg_NoCoinsToReturn, msgBar.INSTANT, true);
                                status = CONFIRMING;
                                break;
                            }
                            deposited -= selectedDrinkData.price;
                            outputer.prepareDrink(drinkSelected, selectedDrinkData.preparationTime * 1000 / Redraw_delay);
                        }
                        else if (deposited){
                            char *buff = new char[strlen(Msg_NotEnoughMoney) + 10];
                            sprintf(buff, Msg_NotEnoughMoney, selectedDrinkData.price - deposited);
                            msgBar.pushMsg(buff, msgBar.INSTANT, true);
                        }
                        else
                            msgBar.pushMsg(Msg_DefaultDisplayDepositEmpty, msgBar.INSTANT, true);
                    }
                    else if (outputer.status == outputer.WAITING)
                        msgBar.pushMsg(Msg_OrderingWithoutTaking, msgBar.INSTANT, true);
                    else
                        msgBar.pushMsg(Msg_DrinkIsStillPrepairing, msgBar.INSTANT, true);
                }
                break;
            case CONFIRMING:
                if (p == ENTER_KEY)
                {
                    deposited -= drinks.drinks[drinkSelected].price;

                    char * buff = new char[300];
                    int i = sprintf(buff, Msg_MoneyReturnBeginText);
                    int **localMoney;
                    localMoney = cashTaker.giveMoney(deposited);
                    bool Comma = false;
                    for (int g = 0; g < cashTaker.totalCoins && localMoney[g]; g++)
                    {
                        if (localMoney[g][1]){
                            if (Comma)
                                i += sprintf(buff + i, Msg_MoneyReturnSeparator);
                            Comma = true;
                            i += sprintf(buff + i, Msg_MoneyReturnRepeater, localMoney[g][1], localMoney[g][0]);
                        }
                    }
                    if (Comma)
                        msgBar.pushMsg(buff, msgBar.INSTANT, true);
                    else
                        msgBar.pushMsg(Msg_ThanksForOrder, msgBar.INSTANT, true);
                    outputer.prepareDrink(drinkSelected, drinks.drinks[drinkSelected].preparationTime * 1000 / Redraw_delay);
                }
                else
                    msgBar.pushMsg(Msg_OrderCanceled, msgBar.INSTANT, true);
                status = DRINK_BLOCK_SELECTED;
                break;
            case CASH_BLOCK_SELECTED:
                switch (p){
                case ENTER_KEY:
                    if (cashTaker.status == cashTaker.HOVER){
                        status = CASH_TAKING;
                        cashTaker.status = cashTaker.SELECTED;
                        msgBar.pushMsg(Msg_StartCashRecieving, msgBar.INSTANT, true);
                        redrawState();
                        while ((money = cashTaker.takeMoney())){
                            deposited += money;
                            char *buff = new char[strlen(Msg_DepositStatus) + 10];
                            sprintf(buff, Msg_DepositStatus, deposited);
                            msgBar.pushMsg(buff, msgBar.INSTANT, true);

                            sprintf(buffForMsg, Msg_DefaultDisplayDeposit, deposited);
                            msgBar.setDefaultMessage(buffForMsg);
                            redrawState();
                        }
                        if (!deposited){
                            msgBar.pushMsg(Msg_CancelingMoneyInput, msgBar.INSTANT, true);
                            msgBar.setDefaultMessage(Msg_DefaultDisplayDepositEmpty);
                        }
                        if (cashTaker.status == cashTaker.UNEXEPTABLE_COIN)
                            msgBar.pushMsg(Msg_UnexeptableCoin, msgBar.INSTANT, true);
                        status = CASH_BLOCK_SELECTED;
                        cashTaker.status = cashTaker.HOVER;
                    }
                    else if (cashTaker.status == cashTaker.HOVER_RETURN){
                        if (!deposited)
                            msgBar.pushMsg(Msg_PushingReturnWithoutDeposit, msgBar.INSTANT, true);
                        else{
                            const int chance = rand() % 100;
                            if (chance < returnChance){
                                char * buff = new char[300];
                                int i = sprintf(buff, Msg_MoneyReturnBeginText);

                                int **localMoney;
                                localMoney = cashTaker.giveMoney(deposited);
                                deposited = 0;
                                bool Comma = false;
                                for (int g = 0; g < cashTaker.totalCoins && localMoney[g]; g++)
                                {
                                    if (localMoney[g][1]){
                                        if (Comma)
                                            i += sprintf(buff + i, Msg_MoneyReturnSeparator);
                                        Comma = true;
                                        i += sprintf(buff + i, Msg_MoneyReturnRepeater, localMoney[g][1], localMoney[g][0]);
                                    }
                                }
                                msgBar.pushMsg(buff, msgBar.INSTANT, true);
                            }
                            else{
                                msgBar.pushMsg(Msg_PushingReturnFailed[rand() % totalFailReturnMessages], msgBar.INSTANT, true);
                            }
                        }
                    }
                    break;
                case BOTTOM_KEY:
                    status = DRINK_BLOCK_SELECTED;
                    cashTaker.status = cashTaker.OFF;
                    drinkSelected = drinks.columns / 2;
                    hoverDrink();
                    break;
                case LEFT_KEY:
                    if (cashTaker.status == cashTaker.HOVER_RETURN)
                    {
                        cashTaker.status = cashTaker.HOVER;
                        msgBar.pushMsg(Msg_HoveringMoneyBlock, msgBar.INSTANT, true);
                    }
                    break;
                case RIGHT_KEY:
                    if (cashTaker.status == cashTaker.HOVER)
                    {
                        cashTaker.status = cashTaker.HOVER_RETURN;
                        if (deposited){
                            char *buff = new char[strlen(Msg_HoveringMoneyReturner) + 10];
                            sprintf(buff, Msg_HoveringMoneyReturner, returnChance);
                            msgBar.pushMsg(buff, msgBar.INSTANT, true);
                        }
                        else
                            msgBar.pushMsg(Msg_HoveringMoneyReturnerWithEmptyDeposit, msgBar.INSTANT, true);
                    }
                    break;
                }
                break;
            case OUTPUT_BLOCK_SELECTED:
                switch (p){
                case ENTER_KEY:
                    if (outputer.status == outputer.WAITING)
                    {
                        outputer.status = outputer.OFF;
                        msgBar.pushMsg(Msg_TakingDrink, msgBar.INSTANT, true);
                        outputer.status == outputer.OFF;
                        drinks.drinks[outputer.preparingID].amount--;
                        drinks.refreshBase();
                        if (drinkSelected >= (int)drinks.TotalDrinks)
                            drinkSelected = drinks.TotalDrinks - 1;
                    }
                    else if (outputer.status == outputer.PREPARING_DRINK)
                        msgBar.pushMsg(Msg_DrinkIsStillPrepairing, msgBar.INSTANT, true);
                    else
                        msgBar.pushMsg(Msg_TakingEmptyDrink, msgBar.INSTANT, true);
                    break;
                case TOP_KEY:
                    status = DRINK_BLOCK_SELECTED;
                    cashTaker.status = cashTaker.OFF;
                    drinkSelected = drinks.TotalDrinks - 1;
                    hoverDrink();
                    break;
                }
                break;
            case SUCCESSFUL_INIT:
                if (p == BOTTOM_KEY || p == LEFT_KEY || p == RIGHT_KEY || p == TOP_KEY){
                    status = CASH_BLOCK_SELECTED;
                    cashTaker.status = cashTaker.HOVER;
                    msgBar.pushMsg(Msg_HoveringMoneyBlock, msgBar.INSTANT, true);
                }
                break;
            }
            if (deposited){
                sprintf(buffForMsg, Msg_DefaultDisplayDeposit, deposited);
                msgBar.setDefaultMessage(buffForMsg);
            }
            else
                msgBar.setDefaultMessage(Msg_DefaultDisplayDepositEmpty);
        };
    }
};



int main()
{
    Automata automata(300, 60, 30, 40);
    automata.turnOn();
    automata.turnOff();
    return 0;
}