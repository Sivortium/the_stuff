// work5_3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
//length of a line, also a maximum of words
const int LENGTH = 100;
const int MAX_LINES = 50;
void messWord(char *start, char *end){
    //this function moves the letters of a word
    for (int i = 1; i < end - start - 1; i++){
        char temp = *(start + i);
        //"i"th letter is swapped to a randomly picked one in a half-interval [start+i; end)
        //[2;8)
        //8-2-1 + 2+1
        //[i,end-start)
        int changedTo = rand() % (end - start - i) + i;
        *(start + i) = *(start + changedTo);
        *(start + changedTo) = temp;
    }
}
int getWords(char *letter, char *arr[LENGTH]){
    //this function makes an array of pointers to the first letter of each word
    //and returns a number of words
    int wordsInd = 0;
    if (*letter != ' ')
        arr[wordsInd++] = letter;

    while (*letter && *letter != '\n'){
        //if it is a letter after ' ' OR before ' '
        //OR if it is the last letter
        if (*letter != ' ' && (*(letter - 1) == ' ' || *(letter + 1) == ' ') || !*(letter + 1) || *(letter + 1) == '\n')
            arr[wordsInd++] = letter;
        letter++;
    }
    return wordsInd;
}
int main()
{
    srand(time(NULL));

    FILE *F = fopen("data.txt", "at+");
    //God, save the memory
    char Arr[MAX_LINES][LENGTH];

    int lines = 0;
    while (lines < MAX_LINES){
        if (!fgets(Arr[lines], LENGTH, F))
        {
            if (!lines){
                printf("At least one line is needed, rewrite your data.txt\n");
                fclose(F);
                exit(0);
            }
            break;
        }
        lines++;
    }

    printf("Writing into the file...:\n");
    fputs("\n\nresult:\n\n", F);
    for (int i = 0; i < lines; i++){
        //array with pointers to the first and the last letters of a word
        char *pointers[LENGTH];
        int words = getWords(Arr[i], pointers);
        //mess the words before they are outputed, 'words' is twice more, than it actually is
        for (int p = 0; p < words; p += 2){
            messWord(pointers[p], pointers[p + 1]);
        }
        int outer = 0;
        //outputing
        while (Arr[i][outer] && Arr[i][outer] != '\n')
            fputc(Arr[i][outer++],F);
        if (i < lines - 1)
            fputc('\n', F);
    }
    fclose(F);
    printf("Done!\n");
    return 0;
}