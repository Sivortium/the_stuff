// work3_3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
int main()
{
    const int MAX_STRING_LENGTH = 80;
    char line[MAX_STRING_LENGTH];
    printf("Input string to parse the words:\n");
    while (!(gets(line))){
        fflush(stdin);
        printf("Something wrong, try again\n");
    }

    int ind = 0;
    char * wordBegining = NULL;
    char * wordEnding = NULL;
    char * curWord;

    //first simbol check, nedeed for "previous space check"
    if (line[ind] != ' ' && line[ind])
    {
        curWord = line;
    }
    ind++;
    while (line[ind])
    {
        //if it is the beginning of the word
        if (line[ind - 1] == ' ' && line[ind] != ' ')
        {
            curWord = line + ind;
        }
        //previous simbol is a letter AND current simbol is space (OR it just the last simbol)
        //so if it is the end of the word
        if (line[ind - 1] != ' ' && (line[ind] == ' ' || !line[ind + 1]))
        {
            //if it is the first word met
            if (!wordEnding){
                wordBegining = curWord;
                wordEnding = line + ind;
            }
            else if (line + ind - curWord > wordEnding - wordBegining)
            {
                //if this word is longer than current long word
                wordBegining = curWord;
                wordEnding = line + ind;
            }
        }
        ind++;
    }
    //outouting the word
    for (int g = 0; g < wordEnding - wordBegining; g++)
    {
        putchar(*(wordBegining + g));
    }
    return 0;
}