// work6_5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <ctime>
int getFib(int n, int *arr)
{
    //this function counts the Nth Fib using cache array
    if (n > 1){
        //if it wasn't already proccessed, put the result in the array
        if (!arr[n - 1])
            arr[n - 1] = getFib(n - 1, arr) + getFib(n - 2, arr);
        return arr[n - 1];
    }
    //returns 0 or 1
    return n;
}
int getSlowFib(int n)
{
    //this function calculates Nth Fib without cache
    if (n > 1)
    {
        return getSlowFib(n - 1) + getSlowFib(n - 2);
    }
    //returns 0 or 1
    return n;
}
int getNthFib(int N)
{
    //an array for results
    int *arr = (int*)malloc(sizeof(int)*N);
    for (int t = 0; t < N; t++)
        arr[t] = 0;
    N = getFib(N, arr);
    free(arr);
    return N;
}
int main()
{
    const int MIN_VALUE = 1;
    const int MAX_VALUE = 41;
    printf("______________________________\n|N\t|F(N)   \t|time |\n------------------------------\n");
    for (int i = MIN_VALUE; i <= MAX_VALUE; i++)
    {
        float time = clock();
        unsigned int result = getSlowFib(i);
        time = clock() - time;
        printf("|%d\t|%10d\t|%.2f|\n", i, result, ((float)time) / CLOCKS_PER_SEC);
    }
    printf("------------------------------\n");
    return 0;
}