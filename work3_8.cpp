#include "stdafx.h"
#include <stdio.h>
int main()
{
    char line[200];
    unsigned int N = 0;
    printf("Input your string:\n");

    while (!gets(line)){
        fflush(stdin);
        printf("Something wrong, try again\n");
    }

    printf("Type in N, number of the word:\n");
    //N > 0
    while (!scanf("%d", &N) || !N){
        fflush(stdin);
        printf("Something wrong, N>0 is needed\n");
    }
    int ind = 1;
    //in case of an error will show the maximum, also it's an index
    int words = 0;
    if (line[0] != ' ')
        words++;

    //if the previous simbol is not the last one
    while (line[ind - 1]){
        if (words == N)
        {
            char simb = line[ind - 1];
            //breaks output if the word is ended(' ' or '/0')
            if (simb != ' ' && simb)
                putchar(simb);
            else
                break;
        }
        else if (words > N){
            break;
        }
        //looks for the first simbol of the word...it is after the outputing part because of the ind=1
        //which is not 0 because of the first spaces
        if (line[ind] != ' ' && line[ind - 1] == ' ')
            words++;
        ind++;
    }
    if (words < N)
        printf("Sorry, the line has only %d words", words);
    printf("\n");
    return 0;
}