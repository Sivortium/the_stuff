// work2_4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
int main()
{
    char line[80];
    printf("Input string to group simbols in it:\n");
    while (!scanf("%s", &line)){
        fflush(stdin);
        printf("Something wrong, try again\n");
    }
    int ind = 0;
    while (line[ind]){
        char letter = line[ind++];
        if (letter >= '0' && letter <= '9')
            printf("%c", letter);
    }
    ind = 0;
    while (line[ind]){
        char letter = line[ind++];
        if (letter < '0' || letter > '9')
            printf("%c", letter);
    }
    printf("\n");
    return 0;
}