﻿// work5_2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>
//dimentions of a 1/4 displayed, W for width, H for height
const int STUFF_W = 40;
const int STUFF_H = 6;

//displayed simbol
const char SIMBOL = '*';
//a space between
const char UNSIMBOL = ' ';

//a chance that SIMBOL will be generated in random, the more CHANCE is, the more of them will be displayed
//(%, example: 25)
const int CHANCE = 10;

//here is the stuff(1/4 of displayed)
char Stuff[STUFF_W][STUFF_H];
void makeIt(){
    //this function is making an array
    for (int w = 0; w < STUFF_W; w++){
        for (int h = 0; h < STUFF_H; h++){
            //25% chance
            if (!(rand() % (100 / CHANCE)))
                Stuff[w][h] = SIMBOL;
            else
                Stuff[w][h] = UNSIMBOL;
        }
    }
}
int main()
{
    while (true){
        makeIt();
        for (int h = 0; h < STUFF_H; h++){
            //left top
            for (int w = 0; w < STUFF_W; w++)
                printf("%c", Stuff[w][h]);
            //right top
            for (int w = STUFF_W - 1; w >= 0; w--)
                printf("%c", Stuff[w][h]);
            putchar('\n');
        }
        for (int h = STUFF_H - 1; h >= 0; h--){
            //right bottom
            for (int w = 0; w < STUFF_W; w++)
                printf("%c", Stuff[w][h]);
            //left bottom
            for (int w = STUFF_W - 1; w >= 0; w--)
                printf("%c", Stuff[w][h]);
            putchar('\n');
        }
        Sleep(1000);
        system("cls");
    }
    return 0;
}