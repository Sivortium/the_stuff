// work3_2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
int main()
{
    const int MAX_STRING_LENGTH = 80;
    char line[MAX_STRING_LENGTH];
    printf("Input string to parse the words:\n");
    while (!(gets(line)))
    {
        fflush(stdin);
        printf("Something wrong, try again\n");
    }

    //needed for ' ' check
    int ind = 1;
    int words = 0;
    bool OUTPUT = false;
    char * wordBeginng;

    //check for the first simbol, nedeed for "previous is space" check
    if (line[0] != ' ' && line[0])
    {
        OUTPUT = true;
        wordBeginng = line;
        putchar(line[0]);
        words++;
    }
    while (line[ind])
    {
        //previous simbol is space and current is not
        if (line[ind - 1] == ' ' && line[ind] != ' ')
        {
            //mark the begining
            wordBeginng = line + ind;
            OUTPUT = true;
            words++;
        }
        //checking for ending output
        if (OUTPUT && (line[ind] == ' ' || !line[ind + 1]))
        {
            OUTPUT = false;
            //if it is the last simbol
            if (!line[ind + 1])
            {
                //output it
                putchar(line[ind]);
                //increment the length
                ind++;
            }
            printf(": %d letters\n", line - wordBeginng + ind);
        }
        //outputing
        if (OUTPUT)
            putchar(line[ind]);
        ind++;
    }
    printf("The line has %d words\n", words);
    return 0;
}