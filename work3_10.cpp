// work3_10.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
int main()
{
    char line[200];
    unsigned int N = 0;
    printf("Input your string:\n");

    while (!gets(line)){
        fflush(stdin);
        printf("Something wrong, try again\n");
    }

    printf("Type in N, number of the word:\n");
    //N > 0
    while (!scanf("%d", &N) || !N){
        fflush(stdin);
        printf("Something wrong, N>0 is needed\n");
    }
    int ind = 1;

    //length of the cutted word, simbobs wich will be copied
    //and then deleted from the end of the line
    int cutted = 0;

    //in case of an error will show the maximum, also it's an index
    int words = 0;

    //trigger needed to delete spaces after the word
    bool deleting_spaces = false;

    //check for first space
    if (line[0] != ' ')
        words++;

    while (line[ind]){
        if (words == N){
            char simb = line[ind];

            if (simb != ' ' && simb && !deleting_spaces)
                cutted++;
            else
                deleting_spaces = true;

            //if the word is found, count the spaces to delete
            if (deleting_spaces && simb == ' ' && simb){
                cutted++;
            }
            else if (deleting_spaces){
                cutted++;
                //move the letters
                int i = ind - cutted;
                while (line[i]){
                    char moved = line[i + cutted];
                    //are we moved all of them?
                    if (!moved){
                        //so delete
                        moved = 0;
                    }
                    line[i] = moved;
                    i++;
                }
                break;
            }
        }
        else if (words > N){
            break;
        }
        //looks for the first simbol of the word...it is after the outputing part because of the ind=1
        //which is not 0 because of the first spaces
        if (line[ind] != ' ' && line[ind - 1] == ' ')
            words++;
        ind++;
    }
    if (words < N)
        printf("Sorry, the line has only %d words", words);
    else{
        printf("Here is your string now:\n%s", line);
    }
    printf("\n");
    return 0;
}