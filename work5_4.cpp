// work5_4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
//length of a line, also a maximum of words
const int LENGTH = 100;
const int MAX_LINES = 50;
FILE *F = fopen("data.txt", "at+");
int getWords(char *letter, char *arr[LENGTH]){
    //this function makes an array of pointers to the first letter of each word
    //and returns a number of words
    int words = 0;
    if (*letter != ' '){
        arr[words] = letter;
        words++;
    }
    letter++;
    while (*(letter - 1) && *(letter - 1) != '\n'){
        if (*letter != ' ' && *(letter - 1) == ' '){
            arr[words] = letter;
            words++;
        }
        letter++;
    }
    return words;
}
void printWord(char *letter){
    //this function prints the letters until it is a ' ' or '\0' on '\n'(fgets)
    while (*letter && *letter != ' ' && *letter != '\n'){
        fputc(*letter++,F);
    }
}
int main()
{
    srand(time(NULL));

    //God, save the memory
    char Arr[MAX_LINES][LENGTH];

    int lines = 0;
    while (lines < MAX_LINES){
        if (!fgets(Arr[lines], LENGTH, F))
        {
            if (!lines){
                printf("At least one line is needed, rewrite your data.txt\n");
                fclose(F);
                exit(0);
            }
            break;
        }
        lines++;
    }

    printf("Writing into the file...:\n");
    fputs("\n\nresult:\n\n", F);

    for (int l = 0; l < lines; l++){
        char *pointers[LENGTH];
        int words = getWords(Arr[l], pointers);
        for (int i = 0; i < words; i++){
            char *temp = pointers[i];
            //"i"th word is swapped to a randomly picked one in a half-interval [i; words)
            int changedTo = rand() % (words - i) + i;
            pointers[i] = pointers[changedTo];
            pointers[changedTo] = temp;
            printWord(pointers[i]);
            //if it is not the last word, place a space
            if (i < words - 1)
                fputc(' ', F);
        }
        if (l < lines - 1)
            fputc('\n', F);
    }
    fclose(F);
    printf("Done!\n");
    return 0;
}