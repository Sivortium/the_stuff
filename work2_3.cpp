// work2_3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
int main()
{
    unsigned int num = 0;
    printf("Input the number of lines\n");
    //num > 0
    while (!scanf("%d", &num) || !num){
        fflush(stdin);
        printf("Something wrong, use numbers only\n");
    }
    //width=2*(num-1)+1;  1-40 - triangle, 41-81 for reversed one
    //total lines
    if (num > 81)
        num=81;
    int cur = num;
    //if more then console width
    if (num > 40)
        cur = 40;
    int g = 0;
    for (g = 0; g < cur; g++){
        printf("%*s", cur-g,"*");
        for (int f = 0; f < g*2; f++)
            printf("*");
        printf("\n");
    }
    //if num > draw in reversed style
    for (g = 1; g < num - cur; g++){
        printf("%*s", g, "*");
        for (int f = 0; f < (cur - g) * 2; f++)
            printf("*");
        printf("\n");
    }
    return 0;
}