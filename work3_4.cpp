// work3_2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>

//max length of the number
const int MAX = 5;

//to show pluses
bool FIRST = 1;
int outputTheWord(char *num){
    int number = 0;
    int length = 0;
    while (num[++length] && length < MAX)
        ;
    //transformation into int
    for (int g = 0; g < length; g++)
    {
        int pow = 1;
        for (int p = length - 1 - g; p >0; p--)
        {
            pow = pow * 10;
        }
        number += (num[g] - '0')*pow;
    }
    //got a number here
    //just for plus
    if (!FIRST){
        printf(" + %d", number);
    }
    else{
        FIRST = false;
        printf("%d", number);
    }
    for (int e = 0; e < MAX; e++)
        num[e] = '\0';
    return number;
}
int main()
{
    char line[200];
    int ind = 0;
    int sum = 0;
    //first letter of the number
    int last = -1;
    char num[MAX] = { '\0' };
    printf("Input string to parse the numbers:\n");
    while (!gets(line)){
        fflush(stdin);
        printf("Something wrong, try again\n");
    }
    printf("Sum = ");
    while (line[ind - 1])
    {
        if (line[ind] <= '9' && line[ind] >= '0')
        {
            //if it is number and the array is full
            if (ind - last == MAX && last != -1){
                sum += outputTheWord(num);
                last = -1;
                //start from here looking for a new number
                ind--;
            }
            else{
                if (last == -1)
                    last = ind;
                num[ind - last] = line[ind];
            }
        }
        else if (last != -1){
            //if the letter is not a number and there are written numbers in num array
            sum += outputTheWord(num);
            last = -1;
        }
        ind++;
    }
    printf(" = %d\n", sum);
    return 0;
}