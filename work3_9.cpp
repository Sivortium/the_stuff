// work3_9.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
int main()
{
    char line[200];
    printf("Input your string:\n");

    while (!gets(line)){
        fflush(stdin);
        printf("Something wrong, try again\n");
    }

    //the first simbol of the output
    int simb = line[0];
    int ind = 0;
    //length
    int q = 0;
    int maxQ = 0;
    while (line[ind]){
        if (line[ind] == line[ind - 1])
            q++;
        else
            q = 1;
        if (q > maxQ){
            maxQ = q;
            simb = line[ind];
        }
        ind++;
    }
    printf("Here is your substring:\n");
    for (int i = 0; i < maxQ; i++)
        putchar(simb);
    printf("\n");
    return 0;
}