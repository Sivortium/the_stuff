// work4_5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main()
{
    //length of a line
    const int LENGTH = 100;
    const int MAX_LINES = 50;

    //output looks better in English
    FILE *F = fopen("data.txt", "at+");
    //God, save the memory
    char Arr[MAX_LINES][LENGTH];
    char *pointers[MAX_LINES];

    int lines = 0;
    while (lines < MAX_LINES){
        if (!fgets(Arr[lines], LENGTH, F))
        {
            if (!lines){
                printf("At least one line is needed, rewrite your data.txt\n");
                fclose(F);
                exit(0);
            }
            break;
        }
        pointers[lines] = Arr[lines];
        lines++;
    }

    printf("Writing into the file...:\n");
    fputs("\n\nresult:\n\n", F);
    for (int i = 0; i < lines; i++){
        int min = i;
        for (int i2 = i + 1; i2 < lines; i2++){
            if (strlen(pointers[min]) > strlen(pointers[i2])){
                min = i2;
            }
        }
        char *temp = pointers[i];
        pointers[i] = pointers[min];
        pointers[min] = temp;
        int outer = 0;
        while (*(pointers[i] + outer) && *(pointers[i] + outer) != '\n'){
            fputc(*(pointers[i] + outer++), F);
        }
        if (i < lines - 1)
            fputc('\n', F);
    }
    fclose(F);
    printf("Done!\n");
    return 0;
}