// work6_8.cpp : Defines the entry point for the console application.
//


#include "stdafx.h"
#define LENGTH 80
#include <stdlib.h>
void evaluate(char * buf, char * operation)
{
    //this function takes pointer to the line(the whole line) and a pointer to operation char(ex pointer to '*')
    int rez = 1;
    int ind = 0;
    //if it is * or / I use 1 as default first argument
    //othewise it is 0
    if (*(operation) == '-' || *(operation) == '+')
    {
        rez = 0;
    }
    //until we reach the beginning of the line
    while (buf < operation - ind)
    {
        ind++;
        //if it is not a number(' ' simbol is ignored)  OR  it is the first simbol
        if ((*(operation - ind - 1) > '9' || *(operation - ind - 1) < '0') && *(operation - ind - 1) != ' ' || buf == operation - ind + 1)
        {
            rez = atoi(operation - ind);
            break;
        }
    }
    //mark the real beginning of the first argument
    buf = operation - ind;
    ind = 0;
    //if there is something after operation simbol, such check is needed for writing the result
    while (*(operation + ind + 1))
    {
        ind++;
        if (*(operation + ind) <= '9' && *(operation + ind) >= '0')
        {
            switch (*(operation))
            {
            case '-':
                rez -= atoi(operation + ind);
                break;
            case '+':
                rez += atoi(operation + ind);
                break;
            case '*':
                rez *= atoi(operation + ind);
                break;
            case '/':
                rez /= atoi(operation + ind);
                break;
            }
            break;
        }
    }
    //transform int into char
    char str[LENGTH] = { 0 };
    sprintf(str, "%d", rez);
    //writing this result instead of operation
    //swap all the rest insols to ' '
    for (int g = 0; g <= operation - buf + ind; g++)
    {
        if (str[g])
            *(buf + g) = str[g];
        else
            *(buf + g) = ' ';
    }
}
void eval(char * buf, char *end)
{
    //this function calculates the buf and writes teh result right into it
    int result = 0;
    int len = 0;
    char *leftBraket = NULL;
    //looking for bracketed substrings
    while (*(buf + len) && len <= end - buf)
    {
        if (*(buf + len) == '(')
        {
            leftBraket = buf + len;
        }
        else if (*(buf + len) == ')')
        {
            *(buf + len) = ' ';
            *leftBraket = ' ';
            //found the substring, calculate it(brakets are down)
            eval(leftBraket, buf + len);
            //look again
            len = -1;
        }
        len++;
    }
    len = 0;
    //next priority is * and / operations
    while (*(buf + len) && len <= end - buf)
    {
        if (*(buf + len) == '*' || *(buf + len) == '/')
        {
            //found one, calculate it, pointer shows where operation simbol is
            evaluate(buf, buf + len);
        }
        len++;
    }
    len = 0;
    //next priority is +, - operations, same stuff
    while (*(buf + len) && len <= end - buf)
    {
        if (*(buf + len) == '+' || *(buf + len) == '-')
        {
            evaluate(buf, buf + len);
        }
        len++;
    }
}
int main(int argc, _TCHAR* argv[])
{
    char line[LENGTH];
    int len = 0;

    //check for brackets
    while (true)
    {
        gets(line);
        int leftBr = 0;
        int rightBr = 0;
        len = 0;
        while (line[len])
        {
            if (line[len] == '(')
                leftBr++;
            else if (line[len] == ')')
                rightBr++;
            len++;
        }
        if (leftBr == rightBr && len)
            break;
        else if (len)
            printf("priority error, check the brackets\n");
        else
            printf("length error, type in something\n");
        fflush(stdin);
    }
    //go count that
    eval(line, line + len - 1);
    //line now has answer only
    printf("%s\n", line);
    //printf("%s = %d", line, eval(line, line + LENGTH));
    return 0;
}