// work2_7.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>

int main()
{
    unsigned char line[200];
    int arr[256] = { 0 };
    printf("Input string see some info about it:\n");
    while (!fgets((char *)line, 300, stdin)){
        fflush(stdin);
        printf("Something wrong, try again\n");
    }
    printf("%s\n", line);
    int ind = 0;
    //ind+1 to exclude \0
    while (line[ind+1])
        arr[line[ind++]]++;

    for (int g = 0; g < 256; g++)
    {
        if (arr[g])
            printf("\"%c\": %d\n", g, arr[g]);
    }
    return 0;
}