// work3_5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main()
{
    //array's limits
    const int MAX_VALUE = 100;
    const int MIN_VALUE = -100;

    unsigned int n;
    printf("Input the N - array's length\n");

    while (!scanf("%d", &n) || n < 2){
        fflush(stdin);
        printf("N needs to be an integer and N>1\n");
    }
    int *arr = NULL;
    arr = (int*)malloc(n*sizeof(int));
    srand(time(NULL));

    //the sum is here
    int sum = 0;

    //count limits
    int from = 0;
    int to = 0;

    for (int i = 0; i < n; i++){
        int num = rand() % (MAX_VALUE - MIN_VALUE + 1) + MIN_VALUE;
        arr[i] = num;
        printf("%d> %d\n", i + 1, num);
        if (num < arr[from]){
            from = i;
        }
        if (num > arr[to]){
            to = i;
        }
    }
    //if the min stands after the biggest
    if (to < from){
        int temp = to;
        to = from;
        from = temp;
    }

    for (int i = from; i <= to; i++){
        sum = sum + arr[i];
    }
    printf("The sum from %d to %d is:\n%d\n", arr[from], arr[to], sum);
    free(arr);
    return 0;
}