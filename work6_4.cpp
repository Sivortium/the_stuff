// work6_4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <ctime>

#define MAX_VALUE 1000000
#define MIN_VALUE -1000000

int recursiveSum(int * arr, int i)
{
    if (!arr[i])
        return arr[i];
    return arr[i] + recursiveSum(arr, i - 1);
}
int commonSum(int * arr, int N)
{
    int p = 0;
    for (int i = 0; i < N; i++)
    {
        p += arr[i];
    }
    return p;
}
int _tmain(int argc, _TCHAR* argv[])
{
    srand(time(NULL));
    int N = 0;
    printf("arrays length is 2^N, type N(<13):\n");
    while (!scanf("%d", &N) || N >= 13)
    {
        printf("something is wrong, try again...\n");
        fflush(stdin);
    }
    int arrayWidth = 1;
    for (int i = 0; i < N; i++)
    {
        arrayWidth *= 2;
    }
    int *arr = (int*)malloc(sizeof(int)*arrayWidth);
    printf("Array length is %d\nFullfiling it with values - [%d,%d] . . . ", arrayWidth, MIN_VALUE, MAX_VALUE);
    for (int i = 0; i < arrayWidth; i++)
        arr[i] = rand() % (MAX_VALUE - MIN_VALUE) + MIN_VALUE;
    printf("done\nStarting to sum it in Recursive way . . . ");
    float time = clock();
    recursiveSum(arr, arrayWidth);
    time = clock() - time;
    printf("done\nCalculated for %.2f cycles\n", ((float)time));
    printf("Starting to sum it in Common way . . . ");
    time = clock();
    commonSum(arr, arrayWidth);
    time = clock() - time;
    printf("done\nCalculated for %.2f cycles\n", ((float)time));
    free(arr);
    putchar('\n');
    return 0;
}