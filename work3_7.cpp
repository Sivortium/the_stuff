// work3_7.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>

int main()
{
    const int MAX_STRING_LENGTH = 80;
    unsigned char line[MAX_STRING_LENGTH];
    const int totalChars = 256;

    int arr[totalChars] = { 0 };
    printf("Input string see some info about it:\n");

    while (!fgets((char *)line, MAX_STRING_LENGTH, stdin)){
        fflush(stdin);
        printf("Something wrong, try again\n");
    }

    int ind = 1;
    //fullfilling the array
    //checking if the next is not \0 (we don't need \n here)
    while (line[ind + 1])
        arr[line[ind++]]++;

    bool FOUND = true;
    //until array is empty
    while (FOUND)
    {
        int max = 0;
        FOUND = false;
        //looking for maximum
        for (int g = 0; g < totalChars; g++)
        {
            if (arr[g]>arr[max])
            {
                max = g;
                FOUND = true;
            }
        }
        //is we found something
        if (FOUND)
        {
            printf("\"%c\": %d\n", max, arr[max]);
            //deleting the maximum
            arr[max] = 0;
        }
    }
    return 0;
}