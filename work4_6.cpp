// work4_6.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
int main()
{
    const int NAME_LENGHT = 50;
    unsigned int n;
    printf("Input the number of your relatives\n");

    while (!scanf("%d", &n) || n < 2){
        fflush(stdin);
        printf("N needs to be an integer and N>1\n");
    }
    char *Names = (char*)malloc(n*sizeof(char)*NAME_LENGHT);

    //pointers
    char *young = Names;
    char *old = Names;
    //values
    int youngV = 500;//let it be the maximum age that any relative can ever archive
    int oldV = 0;

    for (int i = 0; i < n; i++){
        int age = 0;
        printf("The %dth name is...\n", i + 1);
        fflush(stdin);
        fgets(Names + sizeof(char)*NAME_LENGHT*i, NAME_LENGHT, stdin);
        printf("And his age is...\n");
        while (!scanf("%d", &age) || n < 0){
            fflush(stdin);
            printf("I think that the age should to be a positive integer\n");
        }
        if (age < youngV){
            youngV = age;
            young = Names + sizeof(char)*NAME_LENGHT*i;
        }
        if (age > oldV){
            oldV = age;
            old = Names + sizeof(char)*NAME_LENGHT*i;
        }
        fflush(stdin);
    }

    printf("the youngest from these is ");
    int k = 0;
    while (*(young + k))
        putchar(*(young + k++));

    printf("it is %d \nand the oldest from these is ", youngV);
    k = 0;
    while (*(old + k))
        putchar(*(old + k++));
    printf("it is %d \n", oldV);

    free(Names);
    return 0;
}