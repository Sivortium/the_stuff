// work6_3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>

void toString(int num, char * p){
    //this function converts integer into string
    int len = 1;
    int curNum = num;
    
    //until we reach needed power
    //do it until the next  curNum/10^x will be <1...
    while ((int)(curNum / (len * 10)))
        len *= 10;
    
    //place number
    *p = '0' + (int)(num / len);

    //do it again if length can afford it
    if (len > 1)
        toString(num%len, p + 1);
}

int main()
{
    //max numbers of int is 10
    int buf = 0;
    char theString[10] = { 0 };
    while (!scanf("%d", &buf))
    {
        fflush(stdin);
    }
    toString(buf, theString);
    printf("here is your string: %s\n", theString);
    return 0;
}