// work1_3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <math.h>

int main()
{
    printf("Here you can transfer radians into degrees and vise versa\n");
    float num;
    char mode;
    const float PI = atan(1.0) * 4;
    const float DEG = 180;

    while (scanf("%f%c", &num, &mode) != 2 && !(mode == 'd' || mode == 'D' || mode == 'R' || mode == 'r'))
    {
        printf("Something went wrong, try again\n");
        fflush(stdin);
    }

    if (mode == 'd' || mode == 'D'){
        //rad=deg/DEG
        printf("result %fD = %f*PIR = %fR\n", num, num / DEG, num / DEG * PI);
    } else {
        //deg=rad/PI
        printf("result %fR = %fD\n", num / PI);
    }

    return 0;
}