// work1_5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <string.h>

int main()
{
	const int consoleWidth = 80;
    char line[consoleWidth];
    int len = 0;
    printf("type in your string here\n");
    scanf("%s", line);
    printf("%*s\n", consoleWidth/2+strlen(line)/2, line);

    return 0;
}