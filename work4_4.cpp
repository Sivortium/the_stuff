// work4_4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
int main()
{
    char line[200];
    printf("Input your string:\n");

    while (!gets(line)){
        fflush(stdin);
        printf("Something wrong, try again\n");
    }

    //the first simbol of the output
    char *simb = line;
    //index-pointer
    char *ind = line;
    //first-simbol-pointer
    char *simbInd = line;

    //length
    int Q = 0;
    while (*ind){
        //if the simbol has chanched
        if (*(ind + 1) != *ind){
            if (ind - simbInd > Q)
            {
                Q = ind - simbInd;
                simb = ind;
            }
            //save the position of change
            simbInd = ind;
        }
        ind++;
    }
    printf("Here is your substring:\n");
    for (int i = 0; i < Q; i++)
        putchar(*simb);
    printf("\n");
    return 0;
}